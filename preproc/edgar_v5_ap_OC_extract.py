#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Aug  7 19:00:07 2021
Script   to exrtact olny the OC emissions from EDGARv5 process  file
@author: vinodkum
"""

import os

src_dir = '/raven/ptmp/vinodkum/HERMES/datasets/jrc/EDGAR_v5/monthly_mean'
dest_dir = '/raven/ptmp/vinodkum/HERMES/datasets/jrc/EDGAR_v5_oc/monthly_mean'
sectors = [i for i in os.listdir(src_dir) if i.startswith('oc_')]
for sector in sectors:
    filenames = os.listdir(os.path.join(src_dir, sector))
    os.makedirs(os.path.join(dest_dir, sector), exist_ok=True)
    for filename in filenames:
        os.symlink((os.path.join(src_dir, sector, filename)),
                   (os.path.join(dest_dir, sector, filename)))
