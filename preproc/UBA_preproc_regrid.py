#!/usr/bin/env python

# Copyright 2018 Earth Sciences Department, BSC-CNS
#
# This file is part of HERMESv3_GR.
#
# HERMESv3_GR is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# HERMESv3_GR is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with HERMESv3_GR. If not, see <http://www.gnu.org/licenses/>.


import os
from netCDF4 import Dataset
import numpy as np
from warnings import warn as warning
import pyproj
import shutil
from cdo import Cdo
cdo = Cdo()


# ============== README ======================
"""
downloading website: Please contact Stefan Feigenspan: Stefan.Feigenspan@uba.de

Besides citing HERMESv3_GR, users must also acknowledge the use of the corresponding emission inventories in their works
"""

# ============== CONFIGURATION PARAMETERS ======================
INPUT_PATH = '/u/vinodkum/Build_WRF/DATA/UBA'
OUTPUT_PATH = '/raven/ptmp/vinodkum/HERMES/datasets/UBA_regrid/'
LIST_POLLUTANTS = ['BC', 'CO', 'NH3', 'NOX', 'PM10', 'PM2_5', 'SO2', 'NMVOC']
# LIST_POLLUTANTS = ['PM2_5']
# LIST_YEARS = [2016, 2018]
LIST_YEARS = [2018]

# To do yearly emissions
PROCESS_YEARLY = True
YEARLY_INPUT_NAME = '<year>_Sub2020_NFR.nc'

# To process monthly emissions, 2015 directly from monthly_input_name and other years calculated using monthly gridded factors derived from the 2015 monthly data
PROCESS_MONTHLY = False
# MONTHLY_INPUT_NAME = 'monthly/<sector>/v50_<pollutant>_2015_<month>_<sector>.0.1x0.1.nc'
# MONTHLY_PATTERN_FILE = 'temporal_profiles/v432_FM_<sector>.0.1x0.1.nc'
# ==============================================================

"""
Main script to transform UBA regional emission inventory to a NetCDF that follows the CF-1.6 conventions.

This script also calculates the boundaries of the cells and the cell area.

Vinod Kumar (vinod.kumar@mpic.de) from Max Planck Institute for Chemistry (MPIC).
"""

# %%
def ipcc_to_sector_dict():
    # TODO Documentation
    ipcc_sector_dict = {
        "IPCC_1A1a": "ENE",
        "IPCC_1A1b": "REF_TRF",
        "IPCC_1A1c": "REF_TRF",
        "IPCC_1A2a": "IND",
        "IPCC_1A2b": "IND",
        "IPCC_1A2e": "FOO_PAP",
        "IPCC_1A2f": "IND",
        "IPCC_1A2gvii": "IND",
        "IPCC_1A2gviii": "IND",
        "IPCC_1A3ai_i_": "TNR_Aviation_LTO",
        "IPCC_1A3aii_i_": "TNR_Aviation_LTO",
        "IPCC_1A3bi": "TRO_noRES",
        "IPCC_1A3bii": "TRO_noRES",
        "IPCC_1A3biii": "TRO_noRES",
        "IPCC_1A3biv": "TRO_noRES",
        "IPCC_1A3bv": "TRO_noRES",
        "IPCC_1A3bvi": "TRO_RES",
        "IPCC_1A3bvii": "TRO_noRES",
        "IPCC_1A3c": "TNR_Other",
        "IPCC_1A3dii": "TNR_Ship",
        "IPCC_1A3ei": "TNR_Other",
        "IPCC_1A4ai": "RCO",
        "IPCC_1A4aii": "RCO",
        "IPCC_1A4bi": "RCO",
        "IPCC_1A4bii": "RCO",
        "IPCC_1A4ci": "RCO",
        "IPCC_1A4cii": "RCO",
        "IPCC_1A4ciii": "RCO",
        "IPCC_1A5a": "RCO",
        "IPCC_1A5b": "RCO",
        "IPCC_1B1a": "PRO",
        "IPCC_1B1b": "PRO",
        "IPCC_1B2ai": "PRO",
        "IPCC_1B2aiv": "PRO",
        "IPCC_1B2av": "PRO",
        "IPCC_1B2b": "PRO",
        "IPCC_1B2c": "PRO",
        "IPCC_2A1": "NMM",
        "IPCC_2A2": "NMM",
        "IPCC_2A3": "NMM",
        "IPCC_2A5a": "NMM",
        "IPCC_2A5b": "NMM",
        "IPCC_2A6": "NMM",
        "IPCC_2B1": "CHE",
        "IPCC_2B10a": "CHE",
        "IPCC_2B2": "CHE",
        "IPCC_2B3": "CHE",
        "IPCC_2B5": "CHE",
        "IPCC_2B7": "CHE",
        "IPCC_2C1": "IRO",
        "IPCC_2C2": "IRO",
        "IPCC_2C3": "NFE",
        "IPCC_2C5": "NFE",
        "IPCC_2C6": "NFE",
        "IPCC_2C7a": "NFE",
        "IPCC_2C7c": "NFE",
        "IPCC_2D3a": "SOL",
        "IPCC_2D3b": "SOL",
        "IPCC_2D3c": "SOL",
        "IPCC_2D3d": "SOL",
        "IPCC_2D3e": "SOL",
        "IPCC_2D3f": "SOL",
        "IPCC_2D3g": "SOL",
        "IPCC_2D3h": "SOL",
        "IPCC_2D3i": "SOL",
        "IPCC_2G": "NEU",
        "IPCC_2H1": "FOO_PAP",
        "IPCC_2H2": "FOO_PAP",
        "IPCC_2I": "FOO_PAP",
        "IPCC_2L": "NMM",
        "IPCC_3B1a": "MNM",
        "IPCC_3B1b": "MNM",
        "IPCC_3B2": "MNM",
        "IPCC_3B3": "MNM",
        "IPCC_3B4d": "MNM",
        "IPCC_3B4e": "MNM",
        "IPCC_3B4gi": "MNM",
        "IPCC_3B4gii": "MNM",
        "IPCC_3B4giii": "MNM",
        "IPCC_3B4giv": "MNM",
        "IPCC_3Da1": "AGS",
        "IPCC_3Da2a": "AGS",
        "IPCC_3Da2b": "AGS",
        "IPCC_3Da2c": "AGS",
        "IPCC_3Da3": "AGS",
        "IPCC_3Dc": "AGS",
        "IPCC_3De": "AGS",
        "IPCC_3I": "AGS",
        "IPCC_5A": "SWD_INC",
        "IPCC_5B1": "SWD_INC",
        "IPCC_5B2": "SWD_INC",
        "IPCC_5C1bv": "SWD_INC",
        "IPCC_5C2": "SWD_INC",
        "IPCC_5D1": "WWT",
        "IPCC_5E": "SWD_INC",
    }
    return ipcc_sector_dict

# %%
_projections = {}


def unproject(z, l, x, y):
    if z not in _projections:
        _projections[z] = pyproj.Proj(proj='utm', zone=z, ellps='WGS84')
    if l < 'N':
        y -= 10000000
    lng, lat = _projections[z](x, y, inverse=True)
    return (lng, lat)


def create_bounds_2d(coordinates):
    a = 0.5*(coordinates[1:, :]+ coordinates[:-1, :])
    b = np.r_[[coordinates[0, :]-(coordinates[1, :]-coordinates[0, :])], a,
                   [coordinates[-1, :]+(coordinates[-1, :]-coordinates[-2, :])]]
    c = 0.5*(b[:, 1:]+ b[:, :-1])
    d = np.c_[b[:, 0]-(b[:, 1]-b[:, 0]), c,
               b[:, -1]+(b[:, -1]-b[:, -2])]
    bounds = np.dstack((d[:-1, :-1], d[1:, :-1], d[1:, 1:], d[:-1, 1:]))
    return bounds


def create_bounds(coordinates, number_vertices=2):
    """
    Calculate the vertices coordinates.

    :param coordinates: Coordinates in degrees (latitude or longitude)
    :type coordinates: numpy.array

    :param number_vertices: Non mandatory parameter that informs the number of vertices that must have the boundaries.
            (by default 2)
    :type number_vertices: int

    :return: Array with as many elements as vertices for each value of coords.
    :rtype: numpy.array
    """
    interval = coordinates[1] - coordinates[0]

    coords_left = coordinates - interval / 2
    coords_right = coordinates + interval / 2
    bound_coords = np.dstack((coords_left, coords_right))
    return bound_coords


def get_grid_area(filename):
    """
    Calculate the area for each cell of the grid using CDO

    :param filename: Path to the file to calculate the cell area
    :type filename: str

    :return: Area of each cell of the grid.
    :rtype: numpy.array
    """
    s = cdo.gridarea(input=filename)
    nc_aux = Dataset(s, mode='r')
    grid_area = nc_aux.variables['cell_area'][:]
    nc_aux.close()

    return grid_area

def calculate_grid_definition(in_path):
    """
    Calculate the latitude and longitude coordinates of the cell.

    :param in_path: Path to the file that contains all the information.
    :type in_path: str

    :return: Latitudes array, Longitudes array, Latitude bounds, Longitude bounds.
    :rtype: numpy.array, numpy.array, float, float
    """
    with Dataset(in_path) as f:
        utm_lat = f.variables['LAT'][:]
        utm_lon = f.variables['LON'][:]
        geolat = np.zeros((len(utm_lon), len(utm_lat)))
        geolon = np.zeros((len(utm_lon), len(utm_lat)))
        for x, u_lon in enumerate(utm_lon):
            for y, u_lat in enumerate(utm_lat):
                lon_now, lat_now = unproject(32, 'N', u_lon, u_lat)
                geolat[x, y] = lat_now
                geolon[x, y] = lon_now
        lon_bnds = create_bounds_2d(geolon)
        lat_bnds = create_bounds_2d(geolat)

        return utm_lat, utm_lon, geolat, geolon, lon_bnds, lat_bnds


def map_regular_grid(in_path, target_grid, out_path):
    '''
    Opens original UBA emissions file and creates a copy which is regridded
    on a regular lat lon coordinate

    '''
    shutil.copy2(in_path, 'temp.nc')
    utm_lat, utm_lon, geolat, geolon, lon_bnds, lat_bnds = calculate_grid_definition('temp.nc')
    with Dataset('temp.nc', 'r+') as f:
        f.createDimension('nv', 4)
        f.renameDimension('LON', 'rlon')
        f.renameVariable('LON', 'rlon')
        f.renameDimension('LAT', 'rlat')
        f.renameVariable('LAT', 'rlat')
    
        lon_out = f.createVariable('lon', 'f4', ('rlon', 'rlat',))
        lon_out.standard_name = 'longitude'
        lon_out.units = 'degrees_east'
        lon_out.bounds = "lon_bnds"
        lon_out.axis = "X"
        lon_out[:] = geolon[:]

        lon_bnds_out = f.createVariable('lon_bnds', 'f4', ('rlon', 'rlat', 'nv',))
        lon_bnds_out[:] = lon_bnds[:]

        lat_out = f.createVariable('lat', 'f4', ('rlon', 'rlat',))
        lat_out.standard_name = 'latitude'
        lat_out.units = 'degrees_north'
        lat_out.bounds = "lat_bnds"
        lat_out.axis = "Y"
        lat_out[:] = geolat[:]

        lat_bnds_out = f.createVariable('lat_bnds', 'f4', ('rlon', 'rlat', 'nv',))
        lat_bnds_out[:] = lat_bnds[:]

        for var_now in f.variables.keys():
            if var_now not in ['rlon', 'rlat', 'lon', 'lat',
                               'lon_bnds', 'lat_bnds']:
                f[var_now].coordinates = 'lon lat'
    src = cdo.remapcon(target_grid, input='temp.nc', output=out_path)
    # os.system('cdo -O remapcon,{} temp.nc {}'.format(target_grid, out_path))
    os.remove('temp.nc')
    return out_path


# %%
def write_netcdf(output_name_path, data, data_atts, center_lats, center_lons, grid_cell_area, year, sector,
                 month=None):
    # TODO Documentation
    # Creating NetCDF & Dimensions
    print('out: {}'.format(output_name_path))
    nc_output = Dataset(output_name_path, mode='w', format="NETCDF4")
    nc_output.createDimension('nv', 2)
    nc_output.createDimension('lon', center_lons.shape[0])
    nc_output.createDimension('lat', center_lats.shape[0])
    nc_output.createDimension('time', None)

    # TIME
    time = nc_output.createVariable('time', 'd', ('time',), zlib=True)
    # time.units = "{0} since {1}".format(tstep_units, global_atts['Start_DateTime'].strftime('%Y-%m-%d %H:%M:%S'))
    if month is None:
        time.units = "years since {0}-01-01 00:00:00".format(year)
    else:
        time.units = "months since {0}-{1}-01 00:00:00".format(year, str(month).zfill(2))
    time.standard_name = "time"
    time.calendar = "gregorian"
    time.long_name = "time"
    time[:] = [0]

    # LATITUDE
    lat = nc_output.createVariable('lat', 'f', ('lat',), zlib=True)
    lat.bounds = "lat_bnds"
    lat.units = "degrees_north"
    lat.axis = "Y"
    lat.long_name = "latitude"
    lat.standard_name = "latitude"
    lat[:] = center_lats

    lat_bnds = nc_output.createVariable('lat_bnds', 'f', ('lat', 'nv',), zlib=True)
    lat_bnds[:] = create_bounds(center_lats)

    # LONGITUDE
    lon = nc_output.createVariable('lon', 'f', ('lon',), zlib=True)
    lon.bounds = "lon_bnds"
    lon.units = "degrees_east"
    lon.axis = "X"
    lon.long_name = "longitude"
    lon.standard_name = "longitude"
    lon[:] = center_lons

    lon_bnds = nc_output.createVariable('lon_bnds', 'f', ('lon', 'nv',), zlib=True)
    lon_bnds[:] = create_bounds(center_lons)

    # VARIABLE
    nc_var = nc_output.createVariable(data_atts['long_name'], 'f', ('time', 'lat', 'lon',), zlib=True)
    nc_var.units = data_atts['units']
    nc_var.long_name = data_atts['long_name']
    nc_var.coordinates = data_atts['coordinates']
    nc_var.grid_mapping = data_atts['grid_mapping']
    nc_var.cell_measures = 'area: cell_area'
    nc_var[:] = data.reshape((1,) + data.shape)

    # CELL AREA
    cell_area = nc_output.createVariable('cell_area', 'f', ('lat', 'lon',))
    cell_area.long_name = "area of the grid cell"
    cell_area.standard_name = "area"
    cell_area.units = "m2"
    cell_area[:] = grid_cell_area

    # CRS
    crs = nc_output.createVariable('crs', 'i')
    crs.grid_mapping_name = "latitude_longitude"
    crs.semi_major_axis = 6371000.0
    crs.inverse_flattening = 0

    nc_output.setncattr('title', 'UBA inventory for the sector {0} and pollutant {1}'.format(
        sector,  data_atts['long_name']), )
    nc_output.setncattr('Conventions', 'CF-1.6', )
    nc_output.setncattr('institution', 'UBA', )
    nc_output.setncattr('source', 'UBA', )
    nc_output.setncattr('history', 'Re-writing of the UBA input to follow the CF 1.6 conventions;\n' +
                        '2021-05-18: Added time dimension (UNLIMITED);\n' +
                        '2021-05-18: Added boundaries;\n' +
                        '2021-05-18: Added global attributes;\n' +
                        '2021-05-18: Re-naming pollutant;\n' +
                        '2021-05-18: Added cell_area variable;\n')
    nc_output.setncattr('references', 'web: https://www.thru.de/en/thrude/downloads/ \n' +
                        'Contact: Stefan.Feigenspan@uba.de')
    nc_output.setncattr('comment', 'Re-writing done by Vinod Kumar (vinod.kumar@mpic.de) from MPIC ' +
                        '(Max Planck Institute for Chemistry, Mainz)', )

    nc_output.close()



def do_yearly_transformation(year):
    # TODO Documentation, create grid_uba.txt on the go using grid_bounds
    file_path = os.path.join(INPUT_PATH,
                             YEARLY_INPUT_NAME.replace('<year>', str(year)))
    nc_in = map_regular_grid(file_path, 'grid_uba.txt', 'temp2.nc')
    # Reading lat, lon
    grid_area = get_grid_area(nc_in_path)
    with Dataset(nc_in_path, 'r') as nc_in:
        lats = nc_in.variables['lat'][:]
        lons = nc_in.variables['lon'][:]
        for pollutant in LIST_POLLUTANTS:
            data_out = {}
            for ipcc in list(ipcc_to_sector_dict().keys()):
                if 'E_{}_{}'.format(ipcc[5:], pollutant) in nc_in.variables.keys():
                    data_in = nc_in.variables['E_{}_{}'.format(ipcc[5:],
                                                               pollutant.upper().replace('.', '_'))][:]
                    # in kt/year
                    data_in *= 1e6/(365*24*3600)  # in Kg/s
                    data_in /= grid_area  # in Kg/m2/s
    
                    sector = ipcc_to_sector_dict()[ipcc]
                    if sector not in data_out.keys():
                        data_out[sector] = data_in
                    else:
                        data_out[sector] += data_in
                else:
                    warning("The pollutant {0} for the IPCC sector {1} does not exist".format(
                        pollutant, ipcc))
                    
            for sector in set(ipcc_to_sector_dict().values()):
                if sector not in data_out.keys():
                    warning("The pollutant {0} for the sector {1} does not exist".format(
                        pollutant, sector))
                else:
                    data = data_out[sector].filled(0)
                    # Replace fill value of 9e36 with 0
                    if pollutant == 'PM2_5':
                        pollutant = pollutant.replace('_', '')
                    elif pollutant == 'NOX':
                        pollutant = 'nox_no2'
                    data_attributes = {'long_name': pollutant.lower(),
                                       'units': 'kg.m-2.s-1',
                                       'coordinates': 'lat lon',
                                       'grid_mapping': 'crs'}
                    out_path_aux = os.path.join(OUTPUT_PATH, 'yearly_mean',
                                                pollutant.lower() + '_' + sector.lower())
                    if not os.path.exists(out_path_aux):
                        os.makedirs(out_path_aux)
                    write_netcdf(os.path.join(out_path_aux, '{0}_{1}.nc'.format(pollutant.lower(), year)),
                                 data, data_attributes, lats, lons, grid_area, year, sector.lower())    
    os.remove('temp2.nc')
    return True


if __name__ == '__main__':
    do_yearly_transformation(2018)
