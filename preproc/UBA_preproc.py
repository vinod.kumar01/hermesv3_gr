#!/usr/bin/env python

# Copyright 2018 Earth Sciences Department, BSC-CNS
#
# This file is part of HERMESv3_GR.
#
# HERMESv3_GR is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# HERMESv3_GR is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with HERMESv3_GR. If not, see <http://www.gnu.org/licenses/>.


import os
from netCDF4 import Dataset
import numpy as np
from warnings import warn as warning
import pyproj


# ============== README ======================
"""
downloading website: Please contact Stefan Feigenspan: Stefan.Feigenspan@uba.de

Besides citing HERMESv3_GR, users must also acknowledge the use of the corresponding emission inventories in their works
"""

# ============== CONFIGURATION PARAMETERS ======================
INPUT_PATH = '/u/vinodkum/Build_WRF/DATA/UBA'
OUTPUT_PATH = '/raven/ptmp/vinodkum/HERMES/datasets/UBA/'
# LIST_POLLUTANTS = ['BC', 'CO', 'NH3', 'NOX', 'PM10', 'PM2_5', 'SO2', 'NMVOC']
LIST_POLLUTANTS = ['PM2_5']
# LIST_YEARS = [2016, 2018]
LIST_YEARS = [2018]

# To do yearly emissions
PROCESS_YEARLY = False
YEARLY_INPUT_NAME = '<year>_Sub2020_NFR.nc'

# To process monthly emissions, 2015 directly from monthly_input_name and other years calculated using monthly gridded factors derived from the 2015 monthly data
PROCESS_MONTHLY = False
# MONTHLY_INPUT_NAME = 'monthly/<sector>/v50_<pollutant>_2015_<month>_<sector>.0.1x0.1.nc'
# MONTHLY_PATTERN_FILE = 'temporal_profiles/v432_FM_<sector>.0.1x0.1.nc'
# ==============================================================

"""
Main script to transform UBA regional emission inventory to a NetCDF that follows the CF-1.6 conventions.

This script also calculates the boundaries of the cells and the cell area.

Vinod Kumar (vinod.kumar@mpic.de) from Max Planck Institute for Chemistry (MPIC).
"""

# %%
def ipcc_to_sector_dict():
    # TODO Documentation
    ipcc_sector_dict = {
        "IPCC_1A1a": "ENE",
        "IPCC_1A1b": "REF_TRF",
        "IPCC_1A1c": "REF_TRF",
        "IPCC_1A2a": "IND",
        "IPCC_1A2b": "IND",
        "IPCC_1A2e": "FOO_PAP",
        "IPCC_1A2f": "IND",
        "IPCC_1A2gvii": "IND",
        "IPCC_1A2gviii": "IND",
        "IPCC_1A3ai_i_": "TNR_Aviation_LTO",
        "IPCC_1A3aii_i_": "TNR_Aviation_LTO",
        "IPCC_1A3bi": "TRO_noRES",
        "IPCC_1A3bii": "TRO_noRES",
        "IPCC_1A3biii": "TRO_noRES",
        "IPCC_1A3biv": "TRO_noRES",
        "IPCC_1A3bv": "TRO_noRES",
        "IPCC_1A3bvi": "TRO_RES",
        "IPCC_1A3bvii": "TRO_noRES",
        "IPCC_1A3c": "TNR_Other",
        "IPCC_1A3dii": "TNR_Ship",
        "IPCC_1A3ei": "TNR_Other",
        "IPCC_1A4ai": "RCO",
        "IPCC_1A4aii": "RCO",
        "IPCC_1A4bi": "RCO",
        "IPCC_1A4bii": "RCO",
        "IPCC_1A4ci": "RCO",
        "IPCC_1A4cii": "RCO",
        "IPCC_1A4ciii": "RCO",
        "IPCC_1A5a": "RCO",
        "IPCC_1A5b": "RCO",
        "IPCC_1B1a": "PRO",
        "IPCC_1B1b": "PRO",
        "IPCC_1B2ai": "PRO",
        "IPCC_1B2aiv": "PRO",
        "IPCC_1B2av": "PRO",
        "IPCC_1B2b": "PRO",
        "IPCC_1B2c": "PRO",
        "IPCC_2A1": "NMM",
        "IPCC_2A2": "NMM",
        "IPCC_2A3": "NMM",
        "IPCC_2A5a": "NMM",
        "IPCC_2A5b": "NMM",
        "IPCC_2A6": "NMM",
        "IPCC_2B1": "CHE",
        "IPCC_2B10a": "CHE",
        "IPCC_2B2": "CHE",
        "IPCC_2B3": "CHE",
        "IPCC_2B5": "CHE",
        "IPCC_2B7": "CHE",
        "IPCC_2C1": "IRO",
        "IPCC_2C2": "IRO",
        "IPCC_2C3": "NFE",
        "IPCC_2C5": "NFE",
        "IPCC_2C6": "NFE",
        "IPCC_2C7a": "NFE",
        "IPCC_2C7c": "NFE",
        "IPCC_2D3a": "SOL",
        "IPCC_2D3b": "SOL",
        "IPCC_2D3c": "SOL",
        "IPCC_2D3d": "SOL",
        "IPCC_2D3e": "SOL",
        "IPCC_2D3f": "SOL",
        "IPCC_2D3g": "SOL",
        "IPCC_2D3h": "SOL",
        "IPCC_2D3i": "SOL",
        "IPCC_2G": "NEU",
        "IPCC_2H1": "FOO_PAP",
        "IPCC_2H2": "FOO_PAP",
        "IPCC_2I": "FOO_PAP",
        "IPCC_2L": "NMM",
        "IPCC_3B1a": "MNM",
        "IPCC_3B1b": "MNM",
        "IPCC_3B2": "MNM",
        "IPCC_3B3": "MNM",
        "IPCC_3B4d": "MNM",
        "IPCC_3B4e": "MNM",
        "IPCC_3B4gi": "MNM",
        "IPCC_3B4gii": "MNM",
        "IPCC_3B4giii": "MNM",
        "IPCC_3B4giv": "MNM",
        "IPCC_3Da1": "AGS",
        "IPCC_3Da2a": "AGS",
        "IPCC_3Da2b": "AGS",
        "IPCC_3Da2c": "AGS",
        "IPCC_3Da3": "AGS",
        "IPCC_3Dc": "AGS",
        "IPCC_3De": "AGS",
        "IPCC_3I": "AGS",
        "IPCC_5A": "SWD_INC",
        "IPCC_5B1": "SWD_INC",
        "IPCC_5B2": "SWD_INC",
        "IPCC_5C1bv": "SWD_INC",
        "IPCC_5C2": "SWD_INC",
        "IPCC_5D1": "WWT",
        "IPCC_5E": "SWD_INC",
    }
    return ipcc_sector_dict

# %%
_projections = {}


def unproject(z, l, x, y):
    if z not in _projections:
        _projections[z] = pyproj.Proj(proj='utm', zone=z, ellps='WGS84')
    if l < 'N':
        y -= 10000000
    lng, lat = _projections[z](x, y, inverse=True)
    return (lng, lat)


def create_bounds(coordinates):
    a = 0.5*(coordinates[1:, :]+ coordinates[:-1, :])
    b = np.r_[[coordinates[0, :]-(coordinates[1, :]-coordinates[0, :])], a,
                   [coordinates[-1, :]+(coordinates[-1, :]-coordinates[-2, :])]]
    c = 0.5*(b[:, 1:]+ b[:, :-1])
    d = np.c_[b[:, 0]-(b[:, 1]-b[:, 0]), c,
               b[:, -1]+(b[:, -1]-b[:, -2])]
    bounds = np.dstack((d[:-1, :-1], d[1:, :-1], d[1:, 1:], d[:-1, 1:]))
    return bounds


def calculate_grid_definition(in_path):
    """
    Calculate the latitude and longitude coordinates of the cell.

    :param in_path: Path to the file that contains all the information.
    :type in_path: str

    :return: Latitudes array, Longitudes array, Latitude bounds, Longitude bounds.
    :rtype: numpy.array, numpy.array, float, float
    """
    with Dataset(in_path) as f:
        utm_lat = f.variables['LAT'][:]
        utm_lon = f.variables['LON'][:]
        geolat = np.zeros((len(utm_lon), len(utm_lat)))
        geolon = np.zeros((len(utm_lon), len(utm_lat)))
        for x, u_lon in enumerate(utm_lon):
            for y, u_lat in enumerate(utm_lat):
                lon_now, lat_now = unproject(32, 'N', u_lon, u_lat)
                geolat[x, y] = lat_now
                geolon[x, y] = lon_now
        lon_bnds = create_bounds(geolon)
        lon_bnds = np.transpose(lon_bnds, (1, 0, 2))
        lat_bnds = create_bounds(geolat)
        lat_bnds = np.transpose(lat_bnds, (1, 0, 2))

        return utm_lat, utm_lon, geolat.T, geolon.T, lon_bnds, lat_bnds
# %%
# f_out = Dataset('test.nc', mode='w', format="NETCDF4")
# f_out.createDimension('nv', 4)
# f_out.createDimension('glon', geolon.shape[0])
# f_out.createDimension('glat', geolon.shape[1])
# # LATITUDE
# lat = f_out.createVariable('lat', 'f', ('glon', 'glat',), zlib=True)
# lat.bounds = "lat_bnds"
# lat.units = "degrees_north"
# lat.axis = "Y"
# lat.long_name = "latitude"
# lat.standard_name = "latitude"
# lat[:] = geolat
# lat_bnds = f_out.createVariable('lat_bnds', 'f', ('glon', 'glat', 'nv',), zlib=True)
# lat_bnds[:] = create_bounds(geolat)

# # LONGITUDE
# lon = f_out.createVariable('lon', 'f', ('glon', 'glat',), zlib=True)
# lon.bounds = "lon_bnds"
# lon.units = "degrees_east"
# lon.axis = "X"
# lon.long_name = "longitude"
# lon.standard_name = "longitude"
# lon[:] = geolon

# lon_bnds = f_out.createVariable('lon_bnds', 'f', ('glon', 'glat', 'nv',), zlib=True)
# lon_bnds[:] = create_bounds(geolon)

# # dummy_var
# dummy = f_out.createVariable('dummy', 'f', ('glon', 'glat',), zlib=True)
# dummy.coordinates = 'lon lat'
# dummy[:] = np.zeros(geolon.shape, dtype='float')
# f_out.close()
# %%
def write_netcdf(output_name_path, data, data_atts, in_path, year, sector,
                 month=None):
    # TODO Documentation
    # Creating NetCDF & Dimensions
    print('out: {}'.format(output_name_path))
    utm_lat, utm_lon, geolat, geolon, lon_bnds_ap, lat_bnds_ap = calculate_grid_definition(in_path)
    grid_area = np.ones(geolat.shape, dtype=float)*1e6
    nc_output = Dataset(output_name_path, mode='w', format="NETCDF4")
    nc_output.createDimension('nv', 4)
    nc_output.createDimension('rlon', utm_lon.shape[0])
    nc_output.createDimension('rlat', utm_lat.shape[0])
    nc_output.createDimension('time', None)

    # TIME
    time = nc_output.createVariable('time', 'd', ('time',), zlib=True)
    # time.units = "{0} since {1}".format(tstep_units, global_atts['Start_DateTime'].strftime('%Y-%m-%d %H:%M:%S'))
    if month is None:
        time.units = "years since {0}-01-01 00:00:00".format(year)
    else:
        time.units = "months since {0}-{1}-01 00:00:00".format(year, str(month).zfill(2))
    time.standard_name = "time"
    time.calendar = "gregorian"
    time.long_name = "time"
    time[:] = [0]

    # Source LATITUDE
    rlat = nc_output.createVariable('rlat', 'f', ('rlat',), zlib=True)
    rlat.units = "North"
    rlat.axis = "Y"
    rlat.long_name = "Northing"
    rlat.standard_name = "Northing"
    rlat[:] = utm_lat[:]

    # Source LONGITUDE
    rlon = nc_output.createVariable('rlon', 'f', ('rlon',), zlib=True)
    rlon.units = "East"
    rlon.axis = "X"
    rlon.long_name = "Easting"
    rlon.standard_name = "Easting"
    rlon[:] = utm_lon[:]


    # LATITUDE
    lat = nc_output.createVariable('lat', 'f', ('rlat', 'rlon',), zlib=True)
    lat.bounds = "lat_bnds"
    lat.units = "degrees_north"
    lat.axis = "Y"
    lat.long_name = "latitude"
    lat.standard_name = "latitude"
    lat[:] = geolat[:]

    lat_bnds = nc_output.createVariable('lat_bnds', 'f', ('rlat', 'rlon', 'nv',), zlib=True)
    lat_bnds[:] = lat_bnds_ap[:]

    # LONGITUDE
    lon = nc_output.createVariable('lon', 'f', ('rlat', 'rlon',), zlib=True)
    lon.bounds = "lon_bnds"
    lon.units = "degrees_east"
    lon.axis = "X"
    lon.long_name = "longitude"
    lon.standard_name = "longitude"
    lon[:] = geolon[:]

    lon_bnds = nc_output.createVariable('lon_bnds', 'f', ('rlat', 'rlon', 'nv',), zlib=True)
    lon_bnds[:] = lon_bnds_ap[:]

    # VARIABLE
    nc_var = nc_output.createVariable(data_atts['long_name'], 'f', ('time', 'rlat', 'rlon',), zlib=True)
    nc_var.units = data_atts['units']
    nc_var.long_name = data_atts['long_name']
    nc_var.coordinates = data_atts['coordinates']
    nc_var.grid_mapping = data_atts['grid_mapping']
    nc_var.cell_measures = 'area: cell_area'
    nc_var[:] = data.reshape((1,) + data.shape)

    # CELL AREA
    cell_area = nc_output.createVariable('cell_area', 'f', ('rlat', 'rlon',))
    cell_area.long_name = "area of the grid cell"
    cell_area.standard_name = "area"
    cell_area.units = "m2"
    cell_area[:] = np.ones(geolat.shape, dtype=float)*1e6

    # CRS
    crs = nc_output.createVariable('crs', 'i')
    crs.grid_mapping_name = "latitude_longitude"
    crs.semi_major_axis = 6371000.0
    crs.inverse_flattening = 0

    nc_output.setncattr('title', 'UBA inventory for the sector {0} and pollutant {1}'.format(
        sector,  data_atts['long_name']), )
    nc_output.setncattr('Conventions', 'CF-1.6', )
    nc_output.setncattr('institution', 'UBA', )
    nc_output.setncattr('source', 'UBA', )
    nc_output.setncattr('history', 'Re-writing of the UBA input to follow the CF 1.6 conventions;\n' +
                        '2021-05-18: Added time dimension (UNLIMITED);\n' +
                        '2021-05-18: Added boundaries;\n' +
                        '2021-05-18: Added global attributes;\n' +
                        '2021-05-18: Re-naming pollutant;\n' +
                        '2021-05-18: Added cell_area variable;\n')
    nc_output.setncattr('references', 'web: https://www.thru.de/en/thrude/downloads/ \n' +
                        'Contact: Stefan.Feigenspan@uba.de')
    nc_output.setncattr('comment', 'Re-writing done by Vinod Kumar (vinod.kumar@mpic.de) from MPIC ' +
                        '(Max Planck Institute for Chemistry, Mainz)', )

    nc_output.close()

def do_yearly_transformation(year):
    # TODO Documentation
    file_path = os.path.join(INPUT_PATH,
                             YEARLY_INPUT_NAME.replace('<year>', str(year)))
    nc_in = Dataset(file_path, mode='r')
    grid_area = np.ones((len(nc_in['LAT'][:]), len(nc_in['LON'][:])),
                        dtype=float)*1e6
    # Grid aea is 1km2
    for pollutant in LIST_POLLUTANTS:
        data_out = {}
        for ipcc in list(ipcc_to_sector_dict().keys()):
            if 'E_{}_{}'.format(ipcc[5:], pollutant) in nc_in.variables.keys():
                data_in = nc_in.variables['E_{}_{}'.format(ipcc[5:], pollutant.upper().replace('.', '_'))][:]
                # in kt/year
                data_in *= 1e6/(365*24*3600)  # in Kg/s
                data_in *= 1e-6  # in Kg/m2/s  Grid area is 1 km2
                data_in = data_in.T
                sector = ipcc_to_sector_dict()[ipcc]
                if sector not in data_out.keys():
                    data_out[sector] = data_in
                else:
                    data_out[sector] += data_in
            else:
                warning("The pollutant {0} for the IPCC sector {1} does not exist".format(
                    pollutant, ipcc))
                
        for sector in set(ipcc_to_sector_dict().values()):
            if sector not in data_out.keys():
                warning("The pollutant {0} for the sector {1} does not exist".format(
                    pollutant, sector))
            else:
                data = data_out[sector]
                if pollutant == 'PM2_5':
                    pollutant = pollutant.replace('_', '')
                elif pollutant == 'NOx':
                    pollutant = 'nox_no2'
                data_attributes = {'long_name': pollutant.lower(),
                                   'units': 'kg.m-2.s-1',
                                   'coordinates': 'lat lon',
                                   'grid_mapping': 'crs'}
                out_path_aux = os.path.join(OUTPUT_PATH, 'yearly_mean',
                                            pollutant.lower() + '_' + sector.lower())
                if not os.path.exists(out_path_aux):
                    os.makedirs(out_path_aux)
    
                write_netcdf(os.path.join(out_path_aux, '{0}_{1}.nc'.format(pollutant.lower(), year)),
                             data, data_attributes, file_path, year, sector.lower())
    nc_in.close()
    return True


if __name__ == '__main__':
    do_yearly_transformation(2018)
