#!/bin/bash -l
currentIteration=14
lastIteration=13  #7
if [ $currentIteration -gt $lastIteration ]
then 
        echo "Job completed"
        exit 0
else
        echo "current iteration is: " $currentIteration
fi

#conf_file=conf/hermes_d02_edgar5_htap_UBA.conf
conf_file=conf/hermes_d02_edgar5_htap_UBA_mosaic.conf

restartInterval=5   #days
startDate=$(date -d "2018/05/01" +'%Y/%m/%d')
daysOffset=$(($restartInterval*$currentIteration))
echo "number of days since " $startDate "until current iteration is " $daysOffset
startDate=$(date -d "$startDate+$daysOffset days" +'%Y/%m/%d')
echo "start date is " $startDate

startYear=$(date -d "$startDate" +%Y)
startMonth=$(date -d "$startDate" +%m)
startDay=$(date -d "$startDate" +%d)
# update the namelist.input
sed -i '8s/.*/start_date  = '$startYear'\/'$startMonth'\/'$startDay\ 00\:00\:00'/' $conf_file
output_timestep_num=$(($restartInterval*24))
sed -i '13s/.*/output_timestep_num      = '$output_timestep_num'/' $conf_file

echo "hermes_conf has been updated"
submitScript=run_hermes.sh
#run the model
mpirun -np 72 hermesv3_gr -c $conf_file

if [ $? -eq 0 ]
then
  echo "run has completed"
else
  echo "error occured during the srun, exiting"
  exit 1
fi

#update the submit script
currentIteration=$((currentIteration+1))
echo "new iteration is: " $currentIteration

# update the submit file with the new iteration index
sed -i '2s/.*/currentIteration='$currentIteration'/' $submitScript

echo "resubmitting"
./$submitScript

echo "iteration completed"

