#!/usr/bin/env python

# Copyright 2018 Earth Sciences Department, BSC-CNS
#
# This file is part of HERMESv3_GR.
#
# HERMESv3_GR is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# HERMESv3_GR is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with HERMESv3_GR. If not, see <http://www.gnu.org/licenses/>.


from setuptools import find_packages
from setuptools import setup
from hermesv3_gr import __version__


# Get the version number from the relevant file
version = __version__

with open("README.md", "r") as f:
    long_description = f.read()

setup(
    name='hermesv3_gr',
    license='GNU GPL v3',
    # platforms=['GNU/Linux Debian'],
    version=version,
    description='HERMESv3 Global/Regional',
    long_description=long_description,
    long_description_content_type="text/markdown",
    author='Carles Tena Medina',
    author_email='carles.tena@bsc.es',
    url='https://earth.bsc.es/gitlab/es/hermesv3_gr',
    # download_url='https://earth.bsc.es/wiki/doku.php?id=tools:autosubmit',

    keywords=['emissions', 'cmaq', 'monarch', 'wrf-chem', 'atmospheric composition', 'air quality', 'earth science'],
    # setup_requires=['pyproj'],
    install_requires=[
        'numpy',
        'netCDF4>=1.3.1',
        'cdo>=1.3.3',
        'pandas',
        'fiona',
        'Rtree',
        'geopandas',
        'pyproj',
        'configargparse',
        'cf_units>=1.1.3',
        'ESMPy>=7.1.0.dev0',
        'holidays',
        'pytz',
        'timezonefinder>=4.0.0',
        'mpi4py',
    ],
    packages=find_packages(),
    classifiers=[
        "Programming Language :: Python :: 2.7",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
        "Topic :: Scientific/Engineering :: Atmospheric Science"
    ],
    package_data={'': [
        'README.md',
        'CHANGELOG',
        'LICENSE',
    ]
    },
    data_files=[('.', ['LICENSE', 'CHANGELOG', ]),
                ('conf', ['conf/hermes.conf',
                          'conf/EI_configuration.csv', ]),
                ('data', ['data/global_attributes.csv', ]),
                ('data/profiles', []),
                ('data/profiles/speciation', [
                          'data/profiles/speciation/MolecularWeights.csv',
                          'data/profiles/speciation/Speciation_profile_cb05_aero5_Benchmark.csv',
                          'data/profiles/speciation/Speciation_profile_cb05_aero5_CMAQ.csv',
                          'data/profiles/speciation/Speciation_profile_cb05_aero5_MONARCH.csv',
                          'data/profiles/speciation/Speciation_profile_cb05_aero6_CMAQ.csv',
                          'data/profiles/speciation/Speciation_profile_cb05e51_aero6_CMAQ.csv',
                          'data/profiles/speciation/Speciation_profile_radm2_madesorgam_WRF_CHEM.csv', ]),
                ('data/profiles/temporal', [
                          'data/profiles/temporal/TemporalProfile_Daily.csv',
                          'data/profiles/temporal/TemporalProfile_Weekly.csv',
                          'data/profiles/temporal/TemporalProfile_Hourly.csv', 
                          'data/profiles/temporal/TemporalProfile_Monthly.csv',
                          'data/profiles/temporal/tz_world_country_iso3166.csv', ]),
                ('data/profiles/vertical', [
                          'data/profiles/vertical/Benchmark_15layers_vertical_description.csv',
                          'data/profiles/vertical/Vertical_profile.csv', ]),
                ('preproc', ['preproc/ceds_preproc.py',
                             'preproc/eclipsev5a_preproc.py',
                             'preproc/edgarv432_ap_preproc.py',
                             'preproc/edgarv432_voc_preproc.py',
                             'preproc/emep_preproc.py',
                             'preproc/gfas12_preproc.py',
                             'preproc/GFAS_Parameters.csv',
                             'preproc/htapv2_preproc.py',
                             'preproc/tno_mac_iii_preproc.py',
                             'preproc/tno_mac_iii_preproc_voc_ratios.py',
                             'preproc/VOC_split_AIR.csv',
                             'preproc/VOC_split_SHIP.csv',
                             'preproc/wiedinmyer_preproc.py', ]),
                ('preproc/nmvoc', [
                    'preproc/nmvoc/ratio_voc01.nc',
                    'preproc/nmvoc/ratio_voc02.nc',
                    'preproc/nmvoc/ratio_voc03.nc',
                    'preproc/nmvoc/ratio_voc04.nc',
                    'preproc/nmvoc/ratio_voc05.nc',
                    'preproc/nmvoc/ratio_voc06.nc',
                    'preproc/nmvoc/ratio_voc07.nc',
                    'preproc/nmvoc/ratio_voc08.nc',
                    'preproc/nmvoc/ratio_voc09.nc',
                    'preproc/nmvoc/ratio_voc12.nc',
                    'preproc/nmvoc/ratio_voc13.nc',
                    'preproc/nmvoc/ratio_voc14.nc',
                    'preproc/nmvoc/ratio_voc15.nc',
                    'preproc/nmvoc/ratio_voc16.nc',
                    'preproc/nmvoc/ratio_voc17.nc',
                    'preproc/nmvoc/ratio_voc18.nc',
                    'preproc/nmvoc/ratio_voc19.nc',
                    'preproc/nmvoc/ratio_voc20.nc',
                    'preproc/nmvoc/ratio_voc21.nc',
                    'preproc/nmvoc/ratio_voc22.nc',
                    'preproc/nmvoc/ratio_voc23.nc',
                    'preproc/nmvoc/ratio_voc24.nc',
                    'preproc/nmvoc/ratio_voc25.nc', ]),
                ],

    include_package_data=True,

    entry_points={
        'console_scripts': [
            'hermesv3_gr = hermesv3_gr.hermes:run',
            'hermesv3_gr_copy_config_files = hermesv3_gr.tools.sample_files:copy_config_files',
            'hermesv3_gr_copy_preproc_files = hermesv3_gr.tools.sample_files:copy_preproc_files',
            'hermesv3_gr_download_benchmark = hermesv3_gr.tools.download_benchmark:download_benchmark',
        ],
    },
)
