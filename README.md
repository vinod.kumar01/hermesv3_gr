# HERMESv3_GR - High-Elective Resolution Modelling Emission System version 3 – Global_Regional
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/34fc5d6c803444178034b99dd28c7e3c)](https://www.codacy.com/app/carlestena/hermesv3_gr?utm_source=earth.bsc.es&amp;utm_medium=referral&amp;utm_content=gitlab/es/hermesv3_gr&amp;utm_campaign=Badge_Grade)

HERMESv3_GR is a highly customizable emission processing system that calculates emissions from different sources, regions and pollutants over a user-specified global or regional model grid and fulfills the requirements of several CTMs.

The main characteristics of HERMESv3_GR are as follows:
-	User-defined grid and choice between different map projections: Emissions can be computed on any global or regional domain with a regular lat-lon, rotated lat-lon, mercator or lambert conformal conic projection.
-	Choice between different emission inventories: the emission data library of HERMESv3_GR includes current state-of-the-art global and regional inventories that cover different sources (anthropogenic, biomass burning, volcanoes), pollutants (ozone precursor gases, acidifying gases and primary particulates) and base years (past, present and future). Moreover, country-specific scaling and masking factors defined by the user can be applied to the base inventories in order to combine and adjust them.
-	Choice between different vertical, temporal and speciation profiles: HERMESv3_GR includes a dataset of profiles reported by the literature, but it also allows the user to add its own weight factors for any pollutant sector and specie. Additionally, the model is able to combine base inventories with gridded temporal profiles, which can be of importance for those pollutant sectors whose temporal variation is not uniform across the space due to local influences (e.g. residential combustion emissions and temperature).
-	Choice between different CTMs: The generated emission files can be used as input for the CMAQ, WRF-CHEM and NMMB-MONARCH chemical transport models.
-	Choice between different chemical mechanisms: base pollutants can be mapped to several gas-phase and aerosol chemical mechanism, including CB05, CB05e51, RADM2, AERO5, AERO6 and MADE/SORGAM. All these mechanisms are widely used in the air quality modelling community.
-	Parallel implementation: The emission core module of HERMESv3_GR is parallelized using a map partitioning strategy, which allows decreasing the execution time and memory consumption of the model. This feature can be of importance when using the model in operational air quality forecasting systems, for which the simulations need to be completed within the required time constraints.

HERMESv3_GR is an in-house model fully developed by the [Barcelona Supercomputing Center](https://www.bsc.es/).

## Availability

HERMESv3_GR is distributed free of charge under the licence [GNU GPL v3.0](https://www.gnu.org/licenses/quick-guide-gplv3.html).

## Citing

Guevara, M., Tena, C., Porquet, M., Jorba, O., and Pérez García-Pando, C.: HERMESv3, a stand-alone multi-scale atmospheric emission modelling framework – Part 1: global and regional module, Geosci. Model Dev., 12, 1885-1907, https://doi.org/10.5194/gmd-12-1885-2019, 2019.

## Support

Due to our limited time and resources, the developing team can not guarantee a regular support. Nevertheless, we will be happy to provide advice for new users.
Questions should be send to the HERMESv3 mailing list (hermesv3@bsc.es).

In order to join the mailing list, you should send a mail to hermesv3-join@bsc.es (also put marc.guevara@bsc.es) with the following information:

Name, organization and country.